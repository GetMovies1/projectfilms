﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectFilms.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        [Required] [StringLength(200)] public string Name { get; set; }
        [Required] public DateTime DateTime { get; set; }
        //[Required] public double MyRating { get; set; }
        //[Required] public double Rating { get; set; }
        [Required] public bool Gender { get; set; }
        [Required] [StringLength(200)] public string Email { get; set; }
        public virtual ICollection<Film> Films { get; set; }


    }
}