﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectFilms.Models
{
    public class Film
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
        [Required] public string Name { get; set; }
       // [Required] public Guid FilmId { get; set; }
        [Required] public double MyRating { get; set; }
        [Required] public double Rating { get; set; }
        [Required] public string Country { get; set; }
        [Required] public string PictureLink { get; set; }
        [Required] public string Genre { get; set; }
        [Required] public string ImdbId { get; set; }
        [Required] public string Description { get; set; }
        [Required] public string Comment { get; set; }
        // [Required] public I  { get; set; }
        public virtual ICollection<User> Users { get; set; }
        
    }
}